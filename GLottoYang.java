import com.sun.istack.internal.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//이 곳을 수정 했습니다

public class GLottoYang {

    public static void main(String[] args) {
//        메인함수
        MainWindow mainWindow = new MainWindow();
//        mainWindow 라는 생성자함수를 만들고 선언하고있다.
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame창을 닫고 완전히 프로세스까지 깔끔하게 종료해서 닫히도록 해주는 것이다.
        mainWindow.setLayout(new FlowLayout());
//        frame창에서 여섯개의입력값과 로또불러오기버튼값을 정렬해주는것
        JButton mainButton = mainWindow.mBtn;
//       frame에 버튼이다.mbtn은 로또돌려보기라는이름을 가진 JButton이며,mainWindow라는 생성자함수안에 그버튼을 사용하겟다고 선언한것이다
        mainButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int textField = 0; textField < mainWindow.mTextFields.length; textField++) {
                    int checkingNumber = Integer.parseInt(mainWindow.mTextFields[textField].getText());
                    if (checkingNumber > 45 || checkingNumber < 1) {
                        JOptionPane.showMessageDialog(null, "다시입력해주세요. 범위값이 아닙니다.");
                        return;
                    } else if(sameCheckfunction(mainWindow.mInputNumber) && mainWindow.mInputNumber[textField] == 0) {
                        JOptionPane.showMessageDialog(null, "중복값이므로 다시입력해주시길 바랍니다.");
                        return;
                    } else if(sameCheckfunction(mainWindow.mInputNumber) && textField == 5) {
                        JOptionPane.showMessageDialog(null, "중복값이므로 다시입력해주시길 바랍니다.");
                        return;
                    } else {
                        mainWindow.mInputNumber[textField] = checkingNumber;
                    }
                }
                int[] prizeNumber = prizeNumberfunction();
                int rank = prizeFunction(mainWindow.mInputNumber, prizeNumber);
                rankDialog(rank);


                 NewWindow newWindow = new NewWindow(mainWindow.mInputNumber, prizeNumber);
            }
        });
        mainWindow.setSize(350, 300);
        mainWindow.setVisible(true);
    }
    public static void rankDialog(int rank) {
        if(rank == 0) {
            JOptionPane.showMessageDialog(null,"꽝, 입니다, 다시 돌리시겠습니까?");
        }
        else if(rank == 1) {
            JOptionPane.showMessageDialog(null,"6등, 다시 돌리시겠습니까?");
        }
        else if(rank == 2) {
            JOptionPane.showMessageDialog(null,"5등, 다시 돌리시겠습니까?");
        }
        else if(rank == 3) {
            JOptionPane.showMessageDialog(null,"4등, 다시 돌리시겠습니까?");
        }
        else if(rank == 4) {
            JOptionPane.showMessageDialog(null,"3등, 다시 돌리시겠습니까?");
        }
        else if(rank == 5) {
            JOptionPane.showMessageDialog(null,"2등, 다시 돌리시겠습니까?");
        }
        else if(rank == 6) {
            JOptionPane.showMessageDialog(null,"1등, 다시 돌리시겠습니까?");
        }
    }

    /**
     *
     * @return
     */

    public static int[] prizeNumberfunction() {
//        당첨번호를 출력시켜주는 함수이다
        int Array[] = new int[6];
//        당첨번호 6개의값이들어갈 배열을 사용하겟다고 선언한것이다.
        for (int b = 0; b < 6; b++) {
//            0번째부터 5번째까지
            Array[b] = (int) (Math.random() * 45 + 1);
            if(sameCheckfunction(Array) && b !=0) {
                b--;
                continue;
            }
//            당첨번호 가들어갈공간을 Array라두고 0번째부터 5번째까지 랜덤으로 6개를 만들고잇다.
            System.out.println("당첨번호 :" + "\t" + Array[b]);
//            위에값들을 출력시켜주는것이다
        }
        return Array;
        //      배열값을반환.
    }

    /**
     *
     * @param promptArray
     * @param prizeArray
     * @return
     */
    public static int prizeFunction(int[] promptArray, int[] prizeArray) {
        int count = 0;
        for(int prompt_Number = 0; prompt_Number < promptArray.length; prompt_Number++) {
            for(int Random_prizeNumber =0; Random_prizeNumber < prizeArray.length; Random_prizeNumber++) {
                if(prizeArray[Random_prizeNumber] == promptArray[prompt_Number]) {
                    count++;
                    break;
                }
            }
        }
        return count;
    }
    /**
     *
     * @param same
     * @return
     */
    public static boolean sameCheckfunction(int[] same) {
        for (int compare_prompt_number = 0; compare_prompt_number < same.length; compare_prompt_number++) {
            if (compare_prompt_number == 0 || same[compare_prompt_number] == 0) {
                continue;
            }
            for (int two_compare_number = 0; two_compare_number < compare_prompt_number; two_compare_number++) {
                if (same[compare_prompt_number] == same[two_compare_number]) {
                    return true;
                }
            }
        }
        return false;
    }

}


//------------------------------------------------------------------------------------------------------------------------------------------
class MainWindow extends JFrame {
    //    Mainwindow라는 새로운class 이며, JFrame에서 상속받고잇다.
    int numberSix = 6;
    JTextField[] mTextFields = new JTextField[numberSix];
    //    frame에 보여지는 6개의값이들어갈 배열을선언하고잇다.
    JLabel[] mLabels = new JLabel[numberSix];
    //    6개의입력값들이 각각에 몇번째인지를 나타내는 값타이틀이들어갈 배열을 사용하기위해 선언
    JButton mBtn = new JButton("\t" + "로또 돌려보기");
    //    mbtn이라는 이름을 가진 JButton변수이며, text이름은 로또돌려보기이다. 이걸눌르면 이벤트가발생한다.
    int[] mInputNumber = new int[numberSix];


    //     입력받는 값 6개들이 들어갈 공간 =배열
    public MainWindow() {
        String[] numbering = {"첫", "둘", "세", "네", "다섯", "여섯"};

        for (int number = 0; number < numbering.length; number++) {
            mLabels[number] = new JLabel("\t" + numbering[number] + "번쨰 값 :");
            this.add(mLabels[number]);
            mTextFields[number] = new JTextField(20);
            this.add(mTextFields[number]);
        }
        this.add(mBtn);
    }
}
//이 곳을 수정 했습니다
//------------------------------------------------------------------------------------------------------------------------------------------
class NewWindow extends JFrame {
    // 버튼이 눌러지면 만들어지는 새 창을 정의한 클래스
    public NewWindow(@NotNull int[] inputNumbers, int[] randomNumbers) {
        setTitle("새로 띄운 창");

        JPanel NewWindowContainer = new JPanel();
        setContentPane(NewWindowContainer);

//     ContentPane 은 일반적인 컴포넌트를 가질수이슨ㄴ 패널을 설정한다.
        JLabel NewLabel = new JLabel("입력된 번호");
        NewWindowContainer.add(NewLabel);
        for (int number = 0; number < inputNumbers.length; number++) {
            NewWindowContainer.add(new JLabel(String.valueOf(inputNumbers[number])));
//            /이부분이해않감!////
        }
        JLabel PrizeLabel = new JLabel("당첨된 번호 :");
        NewWindowContainer.add(PrizeLabel);
        for(int PN = 0; PN < randomNumbers.length; PN++) {
            NewWindowContainer.add(new JLabel(String.valueOf(randomNumbers[PN])));
        }
        setSize(200, 100);
//        창의 가로세로 크기비율설정
        setResizable(false);
//        frame창이 크기를 조절하지못하도록설정한다
        setVisible(true);
//        frame과 frame안에 내용이 보이도록설정
    }
}